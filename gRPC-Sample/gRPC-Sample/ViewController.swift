//
//  ViewController.swift
//  gRPC-Sample
//
//  Created by Kuriyama Toru on 2017/11/04.
//  Copyright © 2017年 Kuriyama Toru. All rights reserved.
//

import UIKit
import RemoteClient

class ViewController: UIViewController {
    
    let hostAddress = "localhost:50051"

    @IBOutlet weak var field: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        send()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController {
    func send() {
        GRPCCall.useInsecureConnections(forHost: hostAddress)
        GRPCCall.setUserAgentPrefix("HelloWorld/1.0", forHost: hostAddress)
        let client = HLWGreeter(host: hostAddress)
        let request = HLWHelloRequest()
        request.name = "Objective-C-Swift"
        request.message = "Hello, world!"
        
        client.sayHello(with: request, handler: self.recieveResponse)
    }
    
    func recieveResponse(response:HLWHelloReply?, error:Error?) {
        if let sendError = error {
            print(sendError)
            return
        }
        guard let grpcResponse = response else {
            print("Response is nil.")
            return
        }
        print("response:\(grpcResponse.message)")
        field.text = grpcResponse.message
    }
}
